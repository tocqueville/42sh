/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jobs.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:52:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:54:33 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jobs.h"
#include "shell/shell.h"

#include <unistd.h>
#include <signal.h>

int			register_job(t_job *job, int foreground)
{
	t_job	*j;

	if (!job->proc)
	{
		free_job(job);
		return (0);
	}
	if (!(j = g_sh->jobs))
	{
		g_sh->jobs = job;
		job->index = 1;
	}
	else
	{
		while (j->next)
			j = j->next;
		j->next = job;
		job->index = j->index + 1;
	}
	if (!g_sh->interactive)
		return (wait_job(job));
	if (foreground)
		return (job_fg(job, 0));
	job_bg(job, 0);
	return (0);
}

void		register_proc(t_job *job, t_proc *proc, int bltin, int pid)
{
	t_proc	**p;

	if (pid == 0)
	{
		p = &job->proc;
		while (*p)
			p = &(*p)->next;
		*p = proc;
		job->last = proc;
	}
	else
	{
		proc->pid = pid;
		if (!bltin && g_sh->interactive)
		{
			if (!job->pgid)
				job->pgid = pid;
			setpgid(pid, job->pgid);
		}
	}
}

static void	print_job_infos(t_job *job, int opts)
{
	ft_putchar('[');
	ft_putnbr(job->index);
	ft_putchar(']');
	if (job == g_sh->curr)
		ft_putchar('+');
	else if (job == g_sh->prev)
		ft_putchar('-');
	else
		ft_putchar(' ');
	ft_putstr("  ");
	if (opts == 'l')
	{
		ft_putnbr(job->pgid);
		ft_putchar(' ');
	}
}

static void	print_job_state(t_job *job)
{
	static char	*status[] = {"None", "Running", "Stopped", "Done"};

	ft_putstr(status[job->state]);
	if (job->state == JOB_DONE && job->last->ret)
	{
		ft_putchar('(');
		ft_putnbr(job->last->ret);
		ft_putchar(')');
	}
	if (job->state == JOB_STOPPED)
	{
		if (job->last->ret - 128 == SIGSTOP)
			ft_putstr("(SIGSTOP)");
		if (job->last->ret - 128 == SIGTTIN)
			ft_putstr("(SIGTTIN)");
		if (job->last->ret - 128 == SIGTTOU)
			ft_putstr("(SIGTTOU)");
	}
}

t_job		*report_job(t_job *job, int opts)
{
	t_job		*next;

	next = job->next;
	if (opts == 'p')
		ft_putnbr(job->pgid);
	else
	{
		print_job_infos(job, opts);
		print_job_state(job);
		ft_putstr("\t\t");
		print_job_command(job);
		if (job->state == JOB_DONE)
			free_job(job);
	}
	ft_putchar('\n');
	return (next);
}
