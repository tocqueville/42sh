/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:38:36 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <unistd.h>
#include "input/input.h"

static void	shell_sigint(int sig)
{
	(void)sig;
	if (g_in)
	{
		g_in->interrupted = 1;
		ft_putchar('\n');
		write(1, g_in->prompt_default.buf, g_in->prompt_default.len);
	}
}

static void	shell_sigwinch(int sig)
{
	(void)sig;
	if (g_in)
		init_size(g_in);
}

void		setup_signals_shell(int interactive)
{
	signal(SIGQUIT, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);
	signal(SIGTTIN, SIG_IGN);
	signal(SIGTTOU, SIG_IGN);
	signal(SIGWINCH, interactive == 2 ? shell_sigwinch : SIG_IGN);
	signal(SIGINT, interactive == 1 ? shell_sigint : SIG_IGN);
}

void		setup_signals_child(void)
{
	signal(SIGINT, SIG_DFL);
	signal(SIGWINCH, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);
}
