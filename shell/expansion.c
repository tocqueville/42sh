/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/06 18:47:53 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "input/str.h"

static int		expand_home(t_str *res, char **str)
{
	(*str)++;
	if (!**str || **str == '/')
		return (str_insertstr(res, -1, get_env("HOME")));
	return (0);
}

static int		expand_var(t_str *res, char **str)
{
	char	*var;
	size_t	len;
	int		ret;

	(*str)++;
	len = 0;
	if (**str == '{')
		while ((*str)[len] != '}')
			len++;
	else
		while (ft_isalnum((*str)[len]) || (*str)[len] == '_')
			len++;
	if (len == 0)
		return (str_insert(res, -1, '$'));
	if (!(var = ft_strsub(*str, 0, len)))
		return (1);
	ret = str_insertstr(res, -1, get_env(var));
	free(var);
	*str += len - (**str != '{');
	return (ret);
}

static int		escape_str(t_str *res, char **str, char quote)
{
	char	c;

	while ((c = *++*str))
	{
		if (c == '\\')
		{
			c = *++*str;
			if (c != '\\' && c != '$' && !is_quote(c))
				str_insert(res, -1, '\\');
			str_insert(res, -1, c);
			continue ;
		}
		else if (c == '$')
		{
			if (expand_var(res, str))
				return (1);
		}
		else if (c == quote)
			break ;
		else if (str_insert(res, -1, c))
			return (1);
	}
	return (0);
}

static char		*expand_str(char *str)
{
	t_str	res;
	int		ret;

	str_init(&res, NULL);
	if (*str == '~' && expand_home(&res, &str))
		return (NULL);
	while (*str)
	{
		if (*str == '$')
			ret = expand_var(&res, &str);
		else if (*str == '\\')
			ret = str_insert(&res, -1, *++str);
		else if (is_quote(*str))
			ret = escape_str(&res, &str, *str);
		else
			ret = str_insert(&res, -1, *str);
		if (ret)
		{
			free(res.buf);
			return (NULL);
		}
		str++;
	}
	return (res.buf);
}

int				substitute_vars(t_array *words)
{
	char	*s;
	size_t	i;
	t_token *t;

	i = 0;
	while (i < words->size)
	{
		t = words->data[i];
		if (!(s = expand_str(t->val.str)))
			return (-1);
		free(t->val.str);
		t->val.str = s;
		if (!*s)
			ft_arrremove(words, i--);
		else
			words->data[i] = s;
		i++;
	}
	return (words->size == 0);
}
