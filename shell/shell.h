/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/06 18:52:51 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H

# include "libft.h"
# include "shell/env.h"
# include "jobs/jobs.h"
# include "parser/redir.h"

# include <stdlib.h>
# include <termios.h>

# define SH_NAME "21sh"
# define HIST_SIZE 42

typedef struct	s_sh
{
	struct termios	orig_state;
	int				interactive;

	int				run;
	int				pgid;
	t_array			*env;
	t_job			*jobs;
	t_job			*curr;
	t_job			*prev;
	char			*history[HIST_SIZE];
	int				hist_cmd;
}				t_sh;

extern t_sh		*g_sh;

int				init_shell(void);
int				free_shell(void);

int				setup_history(void);
int				save_history(void);
int				push_history(char *cmd);
char			*fetch_history(int *n, int dh);

char			*get_prompt(void);

void			setup_signals_shell(int interactive);
void			setup_signals_child(void);

int				substitute_vars(t_array *words);

int				err(char *message, int ret);
char			*redir_str(t_redir *redir);
char			*proc_str(t_array *words, t_array *redirs, int fg);

#endif
