/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:42:29 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENV_H
# define ENV_H

# include "libft.h"

int		setup_env(void);

char	*get_env(char *name);
int		set_env(char *name, char *value, int overwrite);
int		unset_env(char *name);

int		is_dir(char *path);
char	*get_fullpath(char *file);

int		cmp_envar(char *var, char *name);

#endif
