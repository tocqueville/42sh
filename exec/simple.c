/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simple.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 12:54:50 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "parser/redir.h"

#include <fcntl.h>
#include <unistd.h>

int			is_stdio(int fd, int stdio[3])
{
	int i;

	i = 0;
	while (i < 3)
	{
		if ((stdio[i] == -1 && fd == i) || stdio[i] == fd)
			return (i + 1);
		i++;
	}
	return (0);
}

int			request_fd(int fd, int stdio[3])
{
	int	i;
	int	err;

	err = 0;
	if ((i = is_stdio(fd, stdio)))
	{
		if ((stdio[i - 1] = dup(fd)) == -1)
			err = 1;
		err |= close(fd);
	}
	return (err);
}

int			restore_stdio(t_array *redirs, int stdio[3])
{
	int		err;
	size_t	i;
	t_redir	*r;

	err = 0;
	i = 0;
	while (i < redirs->size)
	{
		r = redirs->data[i++];
		if (r->dest.type != REDIR_CLOSE && !is_stdio(r->redirected, stdio))
			err |= close(r->redirected);
	}
	i = 0;
	while (i < 3)
	{
		if (stdio[i] != -1)
		{
			err |= dup2(stdio[i], i);
			err |= close(stdio[i]);
		}
		i++;
	}
	return (err);
}

static int	do_fork(t_simple *simple, t_job *job, t_proc *p, t_ctx *ctx)
{
	pid_t	pid;

	if ((pid = fork()) == -1)
		return (-1);
	if (pid == 0)
	{
		if (g_sh->interactive)
		{
			pid = getpid();
			job->pgid = job->pgid ? job->pgid : pid;
			setpgid(pid, job->pgid);
			if (!ctx->async)
				tcsetpgrp(0, job->pgid);
			setup_signals_child();
		}
		do_exec(simple->words, simple->redirs, ctx);
	}
	register_proc(job, p, 0, pid);
	return (0);
}

int			exec_simple(t_simple *simple, t_job *job, t_ctx *ctx)
{
	t_proc	*p;
	t_bltin	builtin;

	if (!(p = new_proc(0, simple->words, simple->redirs, !ctx->async)))
		return (1);
	if (substitute_vars(simple->words))
	{
		free_proc(p);
		return (0);
	}
	register_proc(job, p, 0, 0);
	if ((builtin = get_builtin(simple->words->data[0])))
		p->ret = do_builtin(simple, ctx, builtin);
	else if (do_fork(simple, job, p, ctx))
		return (err("Unable to start process", -1));
	p->done = (builtin != NULL);
	return (0);
}
