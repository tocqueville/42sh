/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:21:15 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXEC_H
# define EXEC_H

# include "ctx.h"
# include "shell/shell.h"
# include "parser/ast.h"
# include "builtins/builtins.h"

# define CREAT_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

int		exec(t_cmd *command);
int		exec_cmd(t_cmd *cmd, t_job *job, t_ctx *ctx);

int		exec_simple(t_simple *simple, t_job *job, t_ctx *ctx);
int		exec_binary(t_cmd *cmd, t_job *job, t_ctx *ctx);

void	do_exec(t_array *words, t_array *redirs, t_ctx *ctx);
int		do_builtin(t_simple *simple, t_ctx *ctx, t_bltin bltin);

int		redir_file(t_redir *r);
int		redir_heredoc(t_redir *r);

int		is_stdio(int fd, int stdio[3]);
int		request_fd(int fd, int stdio[3]);
int		restore_stdio(t_array *redirs, int stdio[3]);

#endif
