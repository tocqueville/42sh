/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "parser/redir.h"

#include <fcntl.h>
#include <unistd.h>

int	redir_file(t_redir *r)
{
	char *name;

	name = r->dest.val.file;
	if ((r->dest.val.fd = open(name, r->flags, CREAT_MODE)) == -1)
	{
		if (access(name, F_OK) == 0)
		{
			if (is_dir(name))
				return (fail(SH_NAME, name, "Is a directory", 125));
			else if (access(name, W_OK | R_OK) == -1)
				return (fail(SH_NAME, name, "Permission denied", 125));
			else
				return (fail(SH_NAME, name, "Unexpected error", 125));
		}
		return (fail(SH_NAME, name, "No such file or directory", 125));
	}
	return (0);
}

int	redir_heredoc(t_redir *r)
{
	int fds[2];

	if (pipe(fds))
		return (1);
	if (write(fds[1], r->dest.val.doc.buf, r->dest.val.doc.len) == -1)
		return (1);
	free(r->dest.val.doc.buf);
	r->dest.val.fd = fds[0];
	close(fds[1]);
	return (0);
}
