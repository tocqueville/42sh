/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:48:35 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "libft.h"

#include <unistd.h>

static void	do_exec_pipeline(t_cmd *cmd, t_job *job, t_ctx *ctx, int pfd[2])
{
	ctx->to_close[ctx->num_fd - 1] = pfd[0];
	ctx->pout = pfd[1];
	exec_cmd(cmd->val.binary->left, job, ctx);
	if (ctx->pin != -1)
		close(ctx->pin);
	ctx->pin = pfd[0];
	close(pfd[1]);
}

static int	exec_pipeline(t_cmd *cmd, t_job *job, t_ctx *ctx)
{
	int		pfd[2];
	t_ctx	*child_ctx;

	if (!(child_ctx = dup_ctx(ctx)))
		return (0);
	while (cmd->type == CMD_BINARY && cmd->val.binary->op == '|')
	{
		if (pipe(pfd))
		{
			free_ctx(child_ctx);
			return (err("pipe error", 125));
		}
		do_exec_pipeline(cmd, job, child_ctx, pfd);
		cmd = cmd->val.binary->right;
	}
	child_ctx->num_fd--;
	child_ctx->pout = ctx->pout;
	exec_cmd(cmd, job, child_ctx);
	close(child_ctx->pin);
	free_ctx(child_ctx);
	return (0);
}

static int	exec_both(t_cmd *cmd, t_job *job, t_ctx *ctx, int async)
{
	t_ctx	c;
	t_job	*j;

	if (!(j = new_job()))
		return (255);
	c.pin = ctx->pin;
	c.pout = ctx->pout;
	c.async = async;
	c.num_fd = 0;
	c.to_close = NULL;
	exec_cmd(cmd->val.binary->left, j, &c);
	register_job(j, async ? JOB_BG : JOB_FG);
	if (cmd->val.binary->right)
		return (exec_cmd(cmd->val.binary->right, job, ctx));
	return (0);
}

static int	exec_cond(t_cmd *cmd, t_job *job, t_ctx *ctx, int cond)
{
	t_ctx	c;
	t_job	*j;
	int		ret;

	if (!(j = new_job()))
		return (255);
	c.pin = ctx->pin;
	c.pout = ctx->pout;
	c.async = 0;
	c.num_fd = 0;
	c.to_close = NULL;
	exec_cmd(cmd->val.binary->left, j, &c);
	ret = register_job(j, JOB_FG);
	ret = (cmd->val.binary->left->flags & CMD_INVERT) ? !ret : ret;
	if (cond == TOK_AND_IF)
	{
		if (!ret && cmd->val.binary->right)
			return (exec_cmd(cmd->val.binary->right, job, ctx));
	}
	else
	{
		if ((0 < ret && ret < 128) && cmd->val.binary->right)
			return (exec_cmd(cmd->val.binary->right, job, ctx));
	}
	return (ret);
}

int			exec_binary(t_cmd *cmd, t_job *job, t_ctx *ctx)
{
	int res;

	res = 1;
	if (cmd->val.binary->op == '|')
		res = exec_pipeline(cmd, job, ctx);
	else if (cmd->val.binary->op == ';')
		res = exec_both(cmd, job, ctx, 0);
	else if (cmd->val.binary->op == '&')
		res = exec_both(cmd, job, ctx, 1);
	else if (cmd->val.binary->op == TOK_AND_IF)
		res = exec_cond(cmd, job, ctx, TOK_AND_IF);
	else if (cmd->val.binary->op == TOK_OR_IF)
		res = exec_cond(cmd, job, ctx, TOK_OR_IF);
	return (res);
}
