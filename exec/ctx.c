/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ctx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:31:35 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ctx.h"
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

t_ctx		*dup_ctx(t_ctx *src)
{
	t_ctx	*res;

	if (!(res = malloc(sizeof(t_ctx))))
		return (NULL);
	ft_memcpy(res, src, sizeof(t_ctx));
	res->num_fd++;
	if (!(res->to_close = malloc(res->num_fd * sizeof(int))))
	{
		free(res);
		return (NULL);
	}
	if (src->to_close)
		ft_memcpy(res->to_close, src->to_close, src->num_fd * sizeof(int));
	return (res);
}

void		close_fd_list(t_ctx *ctx)
{
	while (ctx->num_fd)
		close(ctx->to_close[--ctx->num_fd]);
	free(ctx->to_close);
	ctx->to_close = NULL;
}

void		free_ctx(t_ctx *ctx)
{
	if (ctx)
		free(ctx->to_close);
	free(ctx);
}
