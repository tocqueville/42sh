/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ctx.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:31:03 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CTX_H
# define CTX_H

typedef struct	s_ctx
{
	int		pin;
	int		pout;
	int		async;
	int		num_fd;
	int		*to_close;
}				t_ctx;

t_ctx			*dup_ctx(t_ctx *src);
void			close_fd_list(t_ctx *ctx);
void			free_ctx(t_ctx *ctx);

#endif
