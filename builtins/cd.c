/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:21:25 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

int			is_dir(char *path)
{
	struct stat	info;

	if (stat(path, &info) == -1)
		return (0);
	return (S_ISDIR(info.st_mode));
}

static void	print_err(char *name)
{
	if (access(name, F_OK) == 0)
	{
		if (!is_dir(name))
			fail("cd", name, "Not a directory", 1);
		else if (access(name, R_OK) == -1)
			fail("cd", name, "Permission denied", 1);
		else
			fail("cd", name, "Unexpected error", 1);
	}
	else
		fail("cd", name, "No such file or directory", 1);
}

static int	do_cd(char *name)
{
	char *pwd;

	if (name == NULL)
		return (1);
	if (!chdir(name))
	{
		if ((pwd = get_env("PWD")))
			set_env("OLDPWD", pwd, 1);
		pwd = getcwd(NULL, 0);
		set_env("PWD", pwd, 1);
		free(pwd);
		return (0);
	}
	print_err(name);
	return (1);
}

int			sh_cd(int argc, char *const argv[])
{
	int	ret;

	if (argc == 1)
		return (do_cd(get_env("HOME")));
	else if (argc > 2)
		ft_putendl_fd("cd: too many arguments", 2);
	else
	{
		if (ft_strequ(argv[1], "-"))
		{
			if (!(ret = do_cd(get_env("OLDPWD"))))
				ft_putendl(get_env("PWD"));
			return (ret);
		}
		return (do_cd(argv[1]));
	}
	return (1);
}
