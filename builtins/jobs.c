/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jobs.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:28:44 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include "shell/shell.h"
#include "parser/parser.h"

#include <unistd.h>

t_job	*parse_jobspec(char *jobspec)
{
	int	num;

	if (jobspec[0] != '%')
		return (0);
	if ((num = parse_number(jobspec + 1)) != -1)
		return (find_job(num));
	if (jobspec[1] == '%' || jobspec[1] == '+')
		return (g_sh->curr);
	if (jobspec[1] == '-')
		return (g_sh->prev);
	return (NULL);
}

int		parse_opts(int *i, int argc, char *const argv[])
{
	char	*str;
	int		opts;

	opts = 0;
	*i = 1;
	while (*i < argc && argv[*i][0] == '-' && argv[*i][1])
	{
		if (ft_strequ(argv[(*i)++], "--"))
			break ;
		str = argv[(*i) - 1] + 1;
		while (*str)
		{
			if (*str != 'l' && *str != 'p')
				return (err("jobs: usage: jobs [-lp] [jobspec ...]", -1));
			opts = *str;
			str++;
		}
	}
	return (opts);
}

int		sh_jobs(int argc, char *const argv[])
{
	t_job	*j;
	int		i;
	int		opts;
	int		ret;

	ret = 0;
	update_jobs();
	if ((opts = parse_opts(&i, argc, argv)) == -1)
		return (1);
	if (i == argc)
	{
		j = g_sh->jobs;
		while (j)
			j = report_job(j, opts);
	}
	while (i < argc)
	{
		j = parse_jobspec(argv[i++]);
		if (j)
			report_job(j, opts);
		else
			ret = fail("jobs", argv[i - 1], "no such job", 1);
	}
	return (ret);
}

int		sh_fg(int argc, char *const argv[])
{
	t_job	*target;

	update_jobs();
	target = NULL;
	if (argc == 1)
	{
		if (!(target = g_sh->curr))
			return (fail("fg", "current", "no such job", 1));
	}
	else
	{
		if (!(target = parse_jobspec(argv[1])))
			return (fail("fg", argv[1], "no such job", 1));
	}
	if (job_is_done(target))
		return (fail("fg", NULL, "job has terminated", 1));
	print_job_command(target);
	ft_putchar('\n');
	job_fg(target, 1);
	return (0);
}

int		sh_bg(int argc, char *const argv[])
{
	t_job	*target;

	update_jobs();
	target = NULL;
	if (argc == 1)
	{
		if (!(target = g_sh->curr))
			return (fail("bg", "current", "no such job", 1));
	}
	else
	{
		if (!(target = parse_jobspec(argv[1])))
			return (fail("bg", argv[1], "no such job", 1));
	}
	if (job_is_done(target))
		return (fail("bg", NULL, "job has terminated", 1));
	ft_putchar('[');
	ft_putnbr(target->index);
	ft_putstr("] ");
	print_job_command(target);
	ft_putchar('\n');
	job_bg(target, 1);
	return (0);
}
