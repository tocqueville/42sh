/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:34:27 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

static t_builtin	g_builtins[] = {
	{"cd", sh_cd},
	{"echo", sh_echo},
	{"exit", sh_exit},
	{"jobs", sh_jobs},
	{"fg", sh_fg},
	{"bg", sh_bg},
	{"env", sh_env},
	{"setenv", sh_setenv},
	{"unsetenv", sh_unsetenv},
};

t_bltin	get_builtin(char *name)
{
	unsigned i;

	i = 0;
	while (i < (sizeof(g_builtins) / sizeof(t_builtin)))
	{
		if (ft_strcmp(name, g_builtins[i].name) == 0)
			return (g_builtins[i].func);
		i++;
	}
	return (NULL);
}

int		fail(char *proc, char *err, char *message, int ret)
{
	ft_putstr_fd(proc, 2);
	ft_putstr_fd(": ", 2);
	if (err)
	{
		ft_putstr_fd(err, 2);
		ft_putstr_fd(": ", 2);
	}
	ft_putendl_fd(message, 2);
	return (ret);
}
