/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:25:34 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTINS_H
# define BUILTINS_H

# include "shell/env.h"

typedef int		(*t_bltin)(int, char *const[]);

typedef struct	s_builtin
{
	const char	*name;
	t_bltin		func;
}				t_builtin;

t_bltin			get_builtin(char *name);
int				fail(char *proc, char *err, char *message, int ret);

int				sh_cd(int argc, char *const argv[]);
int				sh_echo(int argc, char *const argv[]);
int				sh_exit(int argc, char *const argv[]);

int				sh_jobs(int argc, char *const argv[]);
int				sh_fg(int argc, char *const argv[]);
int				sh_bg(int argc, char *const argv[]);

int				sh_env(int argc, char *const argv[]);
int				sh_setenv(int argc, char *const argv[]);
int				sh_unsetenv(int argc, char *const argv[]);

#endif
