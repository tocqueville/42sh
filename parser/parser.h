/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 12:51:07 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "ast.h"
# include "lexer/lexer.h"

typedef struct	s_parser
{
	t_lexer		lexer;
	t_cmd		*ast;
}				t_parser;

void			init_parser(t_parser *parser);
void			free_parser(t_parser *parser);

int				parse(t_parser *parser);

t_cmd			*parse_complex(t_parser *parser);
t_cmd			*parse_list(t_parser *parser);
t_cmd			*parse_pipeline(t_parser *parser);
t_cmd			*parse_simple(t_parser *parser, char *prompt);

void			*syntax_error(char *str);
t_token			*consume(t_parser *parser, int type);
t_token			*consume_ar(t_parser *parser, int *type);
int				skip_newlines(t_parser *parser);

int				parse_number(char *str);

#endif
