/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 12:53:49 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "exec/exec.h"

void	init_parser(t_parser *parser)
{
	init_lexer(&parser->lexer);
	parser->ast = NULL;
}

void	free_parser(t_parser *parser)
{
	free_lexer(&parser->lexer);
	free_cmd(parser->ast);
}
