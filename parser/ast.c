/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:47:48 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ast.h"
#include "../parser/redir.h"

t_cmd		*new_cmd(int type, int flags)
{
	t_cmd	*cmd;

	if (!(cmd = malloc(sizeof(t_cmd))))
		return (NULL);
	cmd->type = type;
	cmd->flags = flags;
	return (cmd);
}

t_cmd		*new_binary(t_cmd *l, int op, t_cmd *r)
{
	t_cmd	*cmd;

	if (!(cmd = new_cmd(CMD_BINARY, 0)))
		return (NULL);
	if (!(cmd->val.binary = malloc(sizeof(t_binary))))
	{
		free(cmd);
		return (NULL);
	}
	cmd->val.binary->left = l;
	cmd->val.binary->right = r;
	cmd->val.binary->op = op;
	return (cmd);
}

t_cmd		*new_simple(void)
{
	t_cmd	*cmd;

	if ((cmd = new_cmd(CMD_SIMPLE, 0)))
	{
		if ((cmd->val.simple = malloc(sizeof(t_simple))))
		{
			if ((cmd->val.simple->words = ft_arrnew(4, NULL)))
			{
				if ((cmd->val.simple->redirs = ft_arrnew(4, free_redir)))
					return (cmd);
				ft_arrfree(cmd->val.simple->words);
			}
			free(cmd->val.simple);
		}
		free(cmd);
	}
	return (NULL);
}

void		*free_cmd(t_cmd *cmd)
{
	if (!cmd)
		return (NULL);
	if (cmd->type == CMD_BINARY)
	{
		free_cmd(cmd->val.binary->left);
		free_cmd(cmd->val.binary->right);
		free(cmd->val.binary);
	}
	else if (cmd->type == CMD_SIMPLE)
	{
		ft_arrfree(cmd->val.simple->words);
		ft_arrfree(cmd->val.simple->redirs);
		free(cmd->val.simple);
	}
	free(cmd);
	return (NULL);
}
