/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:51:41 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REDIR_H
# define REDIR_H

# include "parser.h"
# include "lexer/token.h"

# define REDIR_FD 0
# define REDIR_FILE 1
# define REDIR_CLOSE 2
# define REDIR_DOC 3

typedef struct	s_rloc
{
	int			type;
	union		u_rloc_val
	{
		int		fd;
		char	*file;
		t_str	doc;
	}			val;
}				t_rloc;

typedef struct	s_redir
{
	int			type;
	int			flags;
	int			redirected;
	t_rloc		dest;
}				t_redir;

t_redir			*parse_redir(t_parser *parser, int *err);
int				parse_heredoc(t_redir *redir);

void			free_redir(t_redir *redir);

#endif
