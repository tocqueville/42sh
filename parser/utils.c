/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:39:49 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "shell/shell.h"

void	*syntax_error(char *str)
{
	ft_putstr_fd(SH_NAME": Syntax error: ", 2);
	ft_putendl_fd(str, 2);
	return (NULL);
}

t_token	*consume(t_parser *parser, int type)
{
	t_token *temp;

	temp = peek_token(&parser->lexer, NULL);
	if (temp && temp->type == type)
		return (pop_token(&parser->lexer, NULL));
	return (NULL);
}

t_token	*consume_ar(t_parser *parser, int *type)
{
	t_token *temp;

	if ((temp = peek_token(&parser->lexer, NULL)))
	{
		while (*type)
		{
			if (temp->type == *type)
				return (pop_token(&parser->lexer, NULL));
			type++;
		}
	}
	return (NULL);
}

int		skip_newlines(t_parser *parser)
{
	t_token *temp;
	t_token *skipped;

	skipped = NULL;
	while ((temp = peek_token(&parser->lexer, NULL))
		&& temp->type == '\n')
		skipped = pop_token(&parser->lexer, NULL);
	return (skipped != NULL);
}

int		parse_number(char *str)
{
	unsigned	res;

	res = 0;
	while (ft_isdigit(*str))
	{
		res = res * 10 + (*str++ - '0');
		if ((long)(int)res != (long)res)
			return (-1);
	}
	if (*str != '\0')
		return (-1);
	return (res);
}
