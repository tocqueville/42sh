/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/06 18:37:43 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"
#include "parser.h"
#include <fcntl.h>

void		free_redir(t_redir *redir)
{
	if (!redir)
		return ;
	if (redir->dest.type == REDIR_DOC)
		free(redir->dest.val.doc.buf);
	free(redir);
}

static int	handle_new_line(t_redir *redir, t_str *input, char *end, size_t len)
{
	ft_arrremove(&g_in->lines, g_in->line);
	g_in->curr = 0;
	if (input->len == len && !ft_memcmp(input->buf, end, len))
	{
		free(input->buf);
		return (1);
	}
	str_insert(input, -1, '\n');
	str_cat(&redir->dest.val.doc, input);
	free(input->buf);
	str_init(input, NULL);
	return (0);
}

static void	do_parse_heredoc(t_redir *redir, char *end, size_t end_len)
{
	char	c;
	t_str	input;

	str_init(&input, NULL);
	while ((c = peek_char(g_in, "heredoc> ", 1)))
	{
		if (c == '\t')
		{
			if (input.len == 0 && redir->type == TOK_DLESSDASH)
				pop_char(g_in, NULL, 1);
		}
		else if (c == '\n')
		{
			if (handle_new_line(redir, &input, end, end_len))
				return ;
		}
		else
			str_insert(&input, -1, pop_char(g_in, NULL, 1));
	}
}

int			parse_heredoc(t_redir *redir)
{
	char	*end;
	int		sl;
	int		sc;

	end = redir->dest.val.file;
	str_init(&redir->dest.val.doc, NULL);
	sl = g_in->line;
	sc = g_in->curr;
	g_in->line++;
	g_in->curr = 0;
	do_parse_heredoc(redir, end, ft_strlen(end));
	if (g_in->interrupted)
		free(redir->dest.val.doc.buf);
	else
	{
		g_in->line = sl;
		g_in->curr = sc;
	}
	return (g_in->interrupted);
}
