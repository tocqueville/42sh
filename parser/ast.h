/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:49:52 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AST_H
# define AST_H

# include "libft.h"

# define CMD_BINARY 0
# define CMD_SIMPLE 1

# define CMD_INVERT (1 << 0)

typedef struct	s_cmd
{
	int					type;
	int					flags;
	union		u_cmd_val
	{
		struct s_binary	*binary;
		struct s_simple	*simple;
	}					val;
}				t_cmd;

typedef struct	s_binary
{
	t_cmd	*left;
	t_cmd	*right;
	int		op;
}				t_binary;

typedef struct	s_simple
{
	t_array	*words;
	t_array	*redirs;
}				t_simple;

t_cmd			*new_cmd(int type, int flags);
t_cmd			*new_binary(t_cmd *l, int op, t_cmd *r);
t_cmd			*new_simple(void);
void			*free_cmd(t_cmd *cmd);

#endif
