/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:22 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:36:03 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKEN_H
# define TOKEN_H

# define TOK_WORD 256
# define TOK_NAME 257
# define TOK_IO_NUMBER 258

/*
** &&  ||  ;;
*/

# define TOK_AND_IF 260
# define TOK_OR_IF 261
# define TOK_DSEMI 262

/*
** <<  >>  <&  >&  <>  <<- >|
*/

# define TOK_DLESS 263
# define TOK_DGREAT 264
# define TOK_LESSAND 265
# define TOK_GREATAND 266
# define TOK_LESSGREAT 267
# define TOK_DLESSDASH 268
# define TOK_CLOBBER 269

# define META_CHARS "\n!#&();<>|"
# define DELIM_WORD " \t\n&();<>|"

typedef struct	s_token
{
	int				type;
	union		u_tok_val
	{
		char		*str;
		int			num;
	}				val;
	struct s_token	*next;
}				t_token;

#endif
