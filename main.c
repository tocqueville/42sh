/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/03/03 11:05:54 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "parser/parser.h"
#include "exec/exec.h"

//TODO: fix norme libft

int		main(int argc, char **argv)
{
	int			ret;
	t_parser	parser;

	(void)argc;
	(void)argv;
	if (init_shell())
		return (0);
	ret = 0;
	while (g_sh->run)
	{
		init_parser(&parser);
		if (parse(&parser))
			ret = exec(parser.ast);
		free_parser(&parser);
	}
	free_shell();
	return (ret);
}
