/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marin <marin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/06 18:34:43 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "input.h"
#include "mapping.h"

static int	read_escape(char seq[5])
{
	if (seq[1] >= 0 && seq[1] <= '9')
	{
		if (read(STDIN_FILENO, seq + 2, 1) != 1)
			return ('\x1b');
		if (seq[2] == '~')
		{
			if (seq[1] == '3')
				return (KEY_DELETE);
		}
		else if (seq[2] == ';')
		{
			if (read(STDIN_FILENO, seq + 3, 2) != 2)
				return ('\x1b');
			if (seq[3] == '5')
				return (ESC_BASE + CTRL(seq[4]));
		}
	}
	else
		return (ESC_BASE + seq[1]);
	return ('\x1b');
}

static int	handle_escape(void)
{
	char seq[5];

	if (read(STDIN_FILENO, &seq[0], 1) != 1)
		return ('\x1b');
	if (seq[0] == '[')
	{
		if (read(STDIN_FILENO, &seq[1], 1) != 1)
			return ('\x1b');
		return (read_escape(seq));
	}
	return (seq[0]);
}

static int	process_enter(int c)
{
	t_str	*l;
	size_t	i;
	char	*n;

	(void)c;
	scroll_view();
	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (g_in->cmd.len)
		str_insert(&g_in->cmd, -1, '\n');
	str_cat(&g_in->cmd, l);
	g_in->lines.data[g_in->lines.size - 1] = NULL;
	g_in->lines.size--;
	i = 0;
	while (i < l->len)
	{
		if ((n = ft_strchr(l->buf + i, '\n')))
			add_line(ft_strsub(l->buf, i, n - l->buf - i));
		else
			break ;
		i = n - l->buf + 1;
	}
	add_line(ft_strsub(l->buf, i, l->len - i));
	str_free(l);
	return (1);
}

static int	read_key(void)
{
	char	c;
	int		nread;

	while ((nread = read(STDIN_FILENO, &c, 1)) != 1)
		if (nread == -1)
			return (-1);
	if (c == '\x1b')
		return (handle_escape());
	return (c);
}

/*
** return values:
** -1: error
** 0: no more
** 1: finished line
** 2: need more
*/

static t_keymap	g_keymaps[MAX_SPECIAL] = {
	[KEY_ENTER] = process_enter,
	[ARROW_LEFT] = move_char,
	[ARROW_RIGHT] = move_char,
	[KEY_HOME] = move_char,
	[KEY_END] = move_char,
	[BACKSPACE] = del_char,
	[KEY_DELETE] = del_char,
	[CTRL_UP] = move_multiline,
	[CTRL_DOWN] = move_multiline,
	[CTRL_RIGHT] = move_word,
	[CTRL_LEFT] = move_word,
	[CTRL('a')] = move_char,
	[CTRL('c')] = discard,
	[CTRL('d')] = discard,
	[CTRL('e')] = move_char,
	[CTRL('f')] = del_word,
	[CTRL('h')] = del_char,
	[CTRL('k')] = cut,
	[CTRL('u')] = cut,
	[CTRL('w')] = del_word,
	[CTRL('y')] = paste,
};

int			process_keypress(void)
{
	int c;

	if ((c = read_key()) == -1)
		return (2);
	if (move_history(c))
		return (2);
	if (0 < c && c < MAX_SPECIAL && g_keymaps[c])
		return (g_keymaps[c](c));
	if (ft_isprint(c))
		insert_char(c);
	return (2);
}
