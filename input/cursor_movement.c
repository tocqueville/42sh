/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_movement.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/03 11:18:55 by agrouard          #+#    #+#             */
/*   Updated: 2019/03/03 11:25:40 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"
#include "mapping.h"

int	move_char(int c)
{
	t_str	*l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == ARROW_LEFT && g_in->cx != 0)
		g_in->cx--;
	else if (c == ARROW_RIGHT && (unsigned)g_in->cx != l->len)
		g_in->cx++;
	else if (c == KEY_HOME || c == CTRL('a'))
		g_in->cx = 0;
	else if (c == KEY_END || c == CTRL('e'))
		g_in->cx = l->len;
	return (2);
}

int	move_word(int c)
{
	t_str	*l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == CTRL_LEFT)
	{
		while (g_in->cx)
			if (ft_isalnum(l->buf[--g_in->cx]))
				break ;
		while (g_in->cx && ft_isalnum(l->buf[g_in->cx - 1]))
			g_in->cx--;
	}
	if (c == CTRL_RIGHT)
	{
		while ((unsigned)g_in->cx != l->len)
			if (ft_isalnum(l->buf[++g_in->cx]))
				break ;
		while ((unsigned)g_in->cx != l->len
				&& ft_isalnum(l->buf[g_in->cx + 1]))
			g_in->cx++;
		if ((unsigned)g_in->cx != l->len)
			g_in->cx++;
	}
	return (2);
}

int	move_multine_real(t_str *l, int c)
{
	char	*n;

	if (c == CTRL_UP)
	{
		n = l->buf + g_in->cx;
		if (*n == '\n')
			n--;
		while (*n != '\n' && n > l->buf)
			n--;
		if (n <= l->buf)
			n = l->buf;
		g_in->cx = n - l->buf;
	}
	if (c == CTRL_DOWN)
	{
		n = l->buf + g_in->cx;
		while (*n != '\n' && n < l->buf + l->len)
			n++;
		if (n >= l->buf + l->len)
			n = l->buf + l->len - 1;
		g_in->cx = n + 1 - l->buf;
	}
	return (2);
}

int	move_multiline(int c)
{
	t_str	*l;
	int		l_len;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	l_len = (int)l->len;
	if (ft_strchr(l->buf, '\n'))
		return (move_multine_real(l, c));
	if (c == CTRL_UP)
		g_in->cx = g_in->cx - g_in->width < 0 ? 0 : g_in->cx - g_in->width;
	if (c == CTRL_DOWN)
	{
		g_in->cx += g_in->width;
		if (g_in->cx > l_len)
			g_in->cx = l_len;
	}
	return (2);
}
