/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chars.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/06 18:32:34 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"
#include "mapping.h"

void	insert_char(int c)
{
	t_str	*l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (!str_insert(l, g_in->cx, c))
		g_in->cx++;
}

int		del_char(int c)
{
	t_str *l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == KEY_DELETE)
	{
		if ((unsigned)g_in->cx == l->len)
			return (2);
		ft_memmove(l->buf + g_in->cx, l->buf + g_in->cx + 1, l->len - g_in->cx);
		l->len--;
	}
	else
	{
		if (g_in->cx == 0)
			return (2);
		ft_memmove(l->buf + g_in->cx - 1, l->buf + g_in->cx,
						l->len - g_in->cx + 1);
		l->len--;
		g_in->cx--;
	}
	return (2);
}

int		del_word(int c)
{
	t_str	*l;
	int		save;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == CTRL('w'))
	{
		save = g_in->cx;
		move_word(CTRL_LEFT);
		set_clipboard(l->buf, g_in->cx, save);
		ft_memmove(l->buf + g_in->cx, l->buf + save, l->len - save + 1);
		l->len -= (save - g_in->cx);
	}
	else if (c == CTRL('f'))
	{
		save = g_in->cx;
		move_word(CTRL_RIGHT);
		set_clipboard(l->buf, save, g_in->cx);
		ft_memmove(l->buf + save, l->buf + g_in->cx, l->len - g_in->cx + 1);
		l->len -= (g_in->cx - save);
		g_in->cx = save;
	}
	return (2);
}
