/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/03/03 11:23:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <unistd.h>
#include "input.h"
#include "shell/shell.h"

t_input	*g_in = NULL;

size_t			prompt_len(char *prompt)
{
	int count;

	count = 0;
	while (*prompt)
	{
		if (*prompt == '\x1b')
			while (*prompt && *prompt != 'm')
				prompt++;
		else
			count++;
		prompt++;
	}
	return (count);
}

void			init_input(t_input *in)
{
	in->interrupted = 0;
	str_init(&in->prompt_default, get_prompt());
	in->prompt_default.capacity = prompt_len(in->prompt_default.buf);
	in->line = 0;
	in->curr = 0;
	ft_arrinit(&in->lines, 1, str_free);
	str_init(&in->cmd, NULL);
	g_in = in;
}

void			free_input(t_input *in)
{
	if (g_sh->interactive && in->cmd.buf)
	{
		in->cmd.buf[in->cmd.len] = '\0';
		push_history(in->cmd.buf);
		str_init(&in->cmd, NULL);
	}
	ft_arrdestroy(&in->lines);
	free(in->prompt_default.buf);
	g_in = NULL;
}

void			init_size(t_input *in)
{
	struct winsize ws;

	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1)
		exit(0);
	in->width = ws.ws_col;
	in->height = ws.ws_row;
	if (in->width == 0 || in->height == 0)
		exit(0);
}

void			init_prompt(t_input *in, char *prompt)
{
	if (!prompt)
		in->prompt = in->prompt_default;
	else
	{
		str_init(&in->prompt, prompt);
		in->prompt.capacity = prompt_len(in->prompt.buf);
	}
	if (g_sh->interactive == 2)
	{
		enable_raw_mode();
		in->hist = 0;
		get_cursor(&in->cx, &in->cy);
		init_size(in);
		if (in->cx)
		{
			ft_putchar('\n');
			in->cx = 0;
			get_cursor(NULL, &in->cy);
		}
	}
}
