/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_new.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/03 11:10:26 by agrouard          #+#    #+#             */
/*   Updated: 2019/03/03 11:10:53 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"

t_str	*str_new(char *buf)
{
	t_str	*str;

	if (!(str = malloc(sizeof(t_str))))
		return (NULL);
	if (buf)
		str_init(str, buf);
	else
	{
		str->capacity = 8;
		str->len = 0;
		if (!(str->buf = ft_strnew(str->capacity)))
		{
			free(str);
			return (NULL);
		}
	}
	return (str);
}

void	str_init(t_str *str, char *buf)
{
	str->len = buf ? ft_strlen(buf) : 0;
	str->capacity = str->len;
	str->buf = buf;
}

void	str_free(t_str *str)
{
	free(str->buf);
	free(str);
}
