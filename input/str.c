/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/03/03 11:10:53 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"

int		str_insert(t_str *str, size_t index, char c)
{
	size_t	capacity;

	if (index > str->len)
		index = str->len;
	if (str->len == str->capacity)
	{
		capacity = str->capacity ? str->capacity * 2 : 8;
		if (!(str->buf = ft_memrealloc(str->buf, str->len, capacity + 1)))
			return (1);
		str->buf[str->len] = '\0';
		str->capacity = capacity;
	}
	ft_memmove(str->buf + index + 1, str->buf + index, str->len + 1 - index);
	str->len++;
	str->buf[index] = c;
	return (0);
}

int		str_insertstr(t_str *str, size_t index, char *s)
{
	char	*temp;
	size_t	len;

	if (!s)
		return (0);
	len = ft_strlen(s);
	if (index > str->len)
		index = str->len;
	if (str->len + len > str->capacity)
	{
		str->capacity = len + str->len;
		if (!(temp = ft_strnew(str->capacity)))
			return (-1);
		ft_memmove(temp, str->buf, index);
	}
	else
		temp = str->buf;
	ft_memmove(temp + index + len, str->buf + index, str->len - index);
	ft_memmove(temp + index, s, len);
	if (temp != str->buf)
		free(str->buf);
	str->buf = temp;
	str->len += len;
	str->buf[str->len] = '\0';
	return (0);
}

int		str_cat(t_str *a, t_str *b)
{
	size_t	capacity;

	capacity = a->len + b->len;
	if (capacity > a->capacity)
	{
		if ((a->buf = ft_memrealloc(a->buf, a->len, capacity + 1)) == NULL)
			return (1);
		a->buf[a->len] = '\0';
		a->capacity = capacity;
	}
	if (!a->buf || !b->buf)
		return (0);
	ft_memmove(a->buf + a->len, b->buf, b->len + 1);
	a->len += b->len;
	return (0);
}
