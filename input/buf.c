/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

int	ab_append(t_abuf *ab, const char *s, size_t len)
{
	char	*new;

	if ((new = (char *)ft_memrealloc(ab->b, ab->len, ab->len + len)) == NULL)
		return (-1);
	ft_memcpy(&new[ab->len], s, len);
	ab->b = new;
	ab->len += len;
	return (0);
}
