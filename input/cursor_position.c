/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_position.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marin <marin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/03/03 11:20:16 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "input.h"
#include "mapping.h"

int	set_cursor(t_abuf *ab, int x, int y)
{
	char	buf[80];
	char	*temp;

	buf[0] = 0;
	ft_strcat(buf, "\x1b[");
	if ((temp = ft_itoa(y + 1)) == NULL)
		return (-1);
	ft_strcat(buf, temp);
	free(temp);
	ft_strcat(buf, ";");
	if ((temp = ft_itoa(x + 1)) == NULL)
		return (-1);
	ft_strcat(buf, temp);
	free(temp);
	ft_strcat(buf, "H");
	return (ab_append(ab, buf, ft_strlen(buf)));
}

int	get_cursor(int *x, int *y)
{
	char		buf[32];
	unsigned	i;
	char		*temp;

	if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4)
		return (-1);
	i = 0;
	while (i < sizeof(buf) - 1)
	{
		if (read(STDIN_FILENO, buf + i, 1) != 1)
			break ;
		if (buf[i] == 'R')
			break ;
		i++;
	}
	buf[i] = 0;
	if (buf[0] != '\x1b' || buf[1] != '[')
		return (-1);
	if (y)
		*y = ft_atoi(&buf[2]) - 1;
	if ((temp = ft_strchr(&buf[2], ';')) == NULL)
		return (-1);
	if (x)
		*x = ft_atoi(temp + 1) - 1;
	return (0);
}
